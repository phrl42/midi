#ifndef __RENDER_H__
#define __RENDER_H__

#include "init.h"

SDL_Texture* renderImageLoadTexture(const char* file);

void renderDisplayTexture(SDL_Texture *texture, int x, int y, int w, int h);

void renderDisplayTextureFlip(SDL_Texture *texture, int x, int y, int w, int h, double angle, SDL_RendererFlip flag);

void renderClear();
void renderPresent();

#endif
