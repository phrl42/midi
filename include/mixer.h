#ifndef __MIXER_H__
#define __MIXER_H__

#include "incs.h"
#include "init.h"

int mixerOpenAudio(int frequency, int channels);

void mixerChangeVolume(int volume);

Mix_Chunk* mixerLoadAudio(const char *file);

int mixerPlayAudio(int channel, Mix_Chunk *sound, int loop);

void mixerFreeChunk(Mix_Chunk *chunk);

#endif
