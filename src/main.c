#include "init.h"
#include "render.h"
#include "font.h"
#include "mixer.h"

#define whiteTiles 8
#define blackTiles 6 

void playMusic(const char *file);

void loadWhite(int index);
void loadBlack(int index);
void resetWhite(int index);
void resetBlack(int index);

Mix_Chunk *keySound;

SDL_Texture *tileW[whiteTiles];
SDL_Texture *tileB[blackTiles];

int main()
{
  bool gameLoop = true; 

  int WIDTH = 1920;
  int HEIGHT = 1080;
  
  int whiteWidth = 50;
  int whiteHeight = 200;

  int blackWidth = 25;
  int blackHeight = 170;

  int count = 0;
  int movage = 0;

  initEngine(0);
  initWindow(WIDTH, HEIGHT, 0, 0, SDL_WINDOW_FULLSCREEN);
  initRenderer(0);
  
  initIMG(IMG_INIT_PNG);

  SDL_Texture *background;

  SDL_Rect startW = { whiteWidth, HEIGHT - whiteHeight, whiteWidth, whiteHeight };
  SDL_Rect startB = { 0, HEIGHT - whiteHeight, blackWidth, blackHeight};
  
  SDL_Event ev;

  mixerOpenAudio(22050, 2);

  background = renderImageLoadTexture("src/img/bg.png");

  for(int i = 0; i < whiteTiles; i++)
  {
    tileW[i] = renderImageLoadTexture("src/img/tileW.png");
  }
  
  for(int i = 0; i < blackTiles; i++)
  {
    tileB[i] = renderImageLoadTexture("src/img/tileB.png");
  }

  while(gameLoop)
  {
    //------RENDER LOOP-----------

    // clear screen
    renderClear();
    
    renderDisplayTexture(background, 0, 0, WIDTH, HEIGHT);
  
    // I don't know what the fuck happened here
    for(int i = 0; i < whiteTiles; i++)
    {
      renderDisplayTexture(tileW[i], ((startW.x * i) + 1 * i) + WIDTH / 3, startW.y, startW.w, startW.h);
      
      if(i < blackTiles + 1 && i > 0)
      {
        count = i - 1;
        if(i == 3)
        {
          movage += whiteWidth;
        }
        movage += 1; 
        renderDisplayTexture(tileB[count], ((whiteWidth * i) - (blackWidth / 2) + movage * i) + WIDTH / 3, startB.y, startB.w, startB.h);
        movage = 0;
      }
    }

    // switch back- with frontbuffer
    renderPresent();

    while(SDL_PollEvent(&ev))
    {
      switch(ev.type)
      {
        case SDL_QUIT:
          gameLoop = false;
          break;

        case SDL_KEYDOWN:
          switch(ev.key.keysym.sym)
          {
            case SDLK_q:
              gameLoop = false;
              break;

            case SDLK_a:
              loadWhite(0);
              playMusic("src/bgm/c.wav");
              break;

            case SDLK_w:
              loadBlack(0);
              playMusic("src/bgm/cs.wav");
              break;

            case SDLK_s:
              loadWhite(1);
              playMusic("src/bgm/d.wav");
              break;

            case SDLK_e:
              loadBlack(1);
              playMusic("src/bgm/ds.wav");
              break;

            case SDLK_d:
              loadWhite(2);
              playMusic("src/bgm/e.wav");
              break;

            case SDLK_f:
              loadWhite(3);
              playMusic("src/bgm/f.wav");
              break;
            
            case SDLK_r:
              loadBlack(3);
              playMusic("src/bgm/fs.wav");
              break;
            
            case SDLK_g:
              loadWhite(4);
              playMusic("src/bgm/g.wav");
              break;

          case SDLK_t:
              loadBlack(4);
              playMusic("src/bgm/gs.wav");
              break;

          case SDLK_h:
              loadWhite(5);
              playMusic("src/bgm/a.wav");
              break;
          
          case SDLK_y:
              loadBlack(5);
              playMusic("src/bgm/as.wav");
              break;
              
          case SDLK_z:
              loadBlack(5);
              playMusic("src/bgm/as.wav");
              break;

          case SDLK_j:
              loadWhite(6);
              playMusic("src/bgm/h.wav");
              break;

          case SDLK_k:
              loadWhite(7);
              playMusic("src/bgm/c4.wav");
              break;

            default:
              break;
          }
          break;
        // end keydown
        case SDL_KEYUP:  
            switch(ev.key.keysym.sym)
            {
              case SDLK_a:
                resetWhite(0);
                break;

              case SDLK_w:
                resetBlack(0);
                break;
              
              case SDLK_s:
                resetWhite(1);
                break;

              case SDLK_e:
                resetBlack(1);
                break;

              case SDLK_d:
                resetWhite(2);
                break;
              
              case SDLK_f:
                resetWhite(3);
                break;

              case SDLK_r:
                resetBlack(3);
                break;
              
              case SDLK_g:
                resetWhite(4);
                break;

              case SDLK_t:
                resetBlack(4);
                break;

              case SDLK_h:
                resetWhite(5);
                break;
              
              case SDLK_y:
                resetBlack(5);
                break;

              case SDLK_z:
                resetBlack(5);
                break;
                
              case SDLK_j:
                resetWhite(6);
                break;

              case SDLK_k:
                resetWhite(7);
                break;

              default:
                break;
            }
        // end keyup
        default:
          break;
      }
    }
  }
  
  mixerFreeChunk(keySound);
  initDestroyTexture(background);

  for(int i = 0; i < whiteTiles; i++)
  {
    initDestroyTexture(tileW[i]);
  }
  
  for(int i = 0; i < blackTiles; i++)
  {
    initDestroyTexture(tileB[i]);
  }

  initQuit();
  return 0;
}

void playMusic(const char *file)
{
  keySound = mixerLoadAudio(file);
  mixerPlayAudio(-1, keySound, 0);
}

void loadWhite(int index)
{
  tileW[index] = renderImageLoadTexture("src/img/tileWP.png");
}

void loadBlack(int index)
{
  tileB[index] = renderImageLoadTexture("src/img/tileBP.png");
}

void resetWhite(int index)
{
  tileW[index] = renderImageLoadTexture("src/img/tileW.png");
}

void resetBlack(int index)
{
  tileB[index] = renderImageLoadTexture("src/img/tileB.png");
}
